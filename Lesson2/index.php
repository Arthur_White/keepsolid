<?php

interface Transport {

    public function move();
    public function getNumOfWheels();
}

class Car implements Transport {

    private $numOfWeels;

    function __construct() {
        $this->numOfWeels = 4;
    }

    public function move() {
        return "on a car!";
    }

    public function getNumOfWheels() {
        return $this->numOfWeels;
    }
}

class Bus implements Transport {

    private $numOfWeels;
    private $numOfPersons;

    function __construct() {
        $this->numOfWeels = 6;
        $this->numOfPersons = 0;
    }

    public function move() {
        return "on a bus!";
    }

    public function getNumOfWheels() {
        return $this->numOfWeels;
    }

    public function getNumOfPersons() {
        return $this->numOfPersons;
    }

    public function pushPersons($numOfPersons) {
        if ($this->numOfPersons < 60){
            $this->numOfPersons += $numOfPersons;
        } else {
            echo "Bus if full! Sorry.";
        }
    }

    public function popPersons($numOfPersons) {
        if ($this->numOfPersons > 0 && $this->numOfPersons >= $numOfPersons) {
            $this->numOfPersons -= $numOfPersons;
        } else if ($this->numOfPersons == 0) {
            echo "Bus is empty.";
        } else {
            echo "There are not so many people on the bus.";
        }
    }
}

class Plane implements Transport {

    private $numOfWeels;
    private $numOfWings;

    function __construct() {
        $this->numOfWeels = 3;
        $this->numOfWings = 2;
    }

    public function move() {
        return "on a plane!";
    }

    public function getNumOfWheels() {
        return $this->numOfWeels;
    }

    public function getNumOfWings() {
        return $this->numOfWings;
    }
}

// Car
$car = new Car();
echo "Move ".$car->move()." And it's have a ".$car->getNumOfWheels()." wheels!<br><br>";

// Bus
$bus = new Bus();
echo "Move ".$bus->move()." And it's have a ".$bus->getNumOfWheels()." wheels!<br>";

echo "Add 15 persons in a bus!<br>";
$bus->pushPersons(15);

echo "In a bus ".$bus->getNumOfPersons()." persons <br>";

echo "Remove 5 persons<br>";
$bus->popPersons(5);

echo "In a bus ".$bus->getNumOfPersons()." persons <br><br>";

// Plane
$plane = new Plane();
echo "Move ".$plane->move()." And it's have a ".$plane->getNumOfWheels()." wheels, but need wings!<br>";
echo "And it's have ".$plane->getNumOfWings()." wings";



